package com.biom4st3r.utilgolem;

import com.biom4st3r.biow0rks.entities.EntityPacket;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;

/**
 * ModInitClient
 */
public class ModInitClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        EntityPacket.client_RegisterEntityPacket(UtilityGolemEntity.packetid);
        EntityRendererRegistry.INSTANCE.register(
            ModInit.Utility_Golem, 
            (manager,context)->
            {
                return new UtilityGolemEntityRenderer2(manager, new UtilityGolemModel2(), 1);
                // return new UtilityGolemEntityRenderer(manager, new UtilityGolemModel<>(), 1);
            });
    }

    
}