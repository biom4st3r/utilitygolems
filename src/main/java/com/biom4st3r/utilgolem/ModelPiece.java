package com.biom4st3r.utilgolem;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.fabricmc.fabric.impl.client.texture.FabricSprite;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.json.ModelItemPropertyOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.resource.metadata.AnimationResourceMetadata;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.Sprite.Info;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;

/**
 * FabricModelPiece
 */
public class ModelPiece implements FabricBakedModel, BakedModel {
    int textureWidth, textureHeight, modelWidth, modelDepth, modelHeight;
    float x, y, z;
    Identifier texture;
    int defaultU, defaultV;
    SpriteAtlasTexture atlas;
    Sprite.Info info;
    NativeImage ni;

    public ModelPiece(Identifier id, int defaultU, int defaultV, float x, float y, float z, int modelWidth,
            int modelDepth, int modelHeight, int textureWidth, int textureHeight) {
        this.texture = id;
        this.defaultU = defaultU;
        this.defaultV = defaultV;
        this.x = x;
        this.y = y;
        this.z = z;
        this.modelWidth = modelWidth;
        this.modelDepth = modelDepth;
        this.modelHeight = modelHeight;
        this.textureWidth = textureWidth;
        this.textureHeight = textureHeight;
        atlas = new SpriteAtlasTexture(texture);
        this.info = new Info(texture, textureWidth, textureHeight, AnimationResourceMetadata.EMPTY);
        ni = new NativeImage(textureWidth, textureHeight, true);
    }

    public Sprite spriteGetter(int u, int v, int width, int height) {
        return new FabricSprite(atlas, info, 1, u, v, width, height, ni);
    }

    @Override
    public boolean isVanillaAdapter() {

        // FabricSprite sprite = new FabricSprite(atlas, new Sprite.Info(texture,
        // textureWidth, textureHeight, AnimationResourceMetadata.EMPTY), 0, u, v, x, y,
        // null);
        return false;
    }

    @Override
    public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos,
            Supplier<Random> randomSupplier, RenderContext context) {
        // float scale = 16.0F;
        // context.getEmitter()
        // .pos(0, x/scale, (scale-y), z/scale)
        // .pos(1, x/scale,(scale-y)/scale,(z+modelHeight)/scale)
        // .pos(2,(x+modelWidth)/scale,(scale-y)/scale,z/scale)
        // .pos(3,(x+modelWidth)/scale,(scale-y)/scale,z/scale)
        // .spriteBake(0, spriteGetter(defaultU+modelWidth, defaultV, modelWidth,
        // modelDepth), 0);
        // .spriteBake(0, spriteGetter(defaultU+modelWidth, defaultV+modelDepth,
        // modelWidth, modelHeight), 0)
        int tU = defaultU;
        int tV = defaultV;
        context.getEmitter().square(Direction.EAST, z, y, z + modelDepth, y + modelHeight, x)
                .spriteBake(0, spriteGetter(tU, (tV += modelHeight), modelDepth, modelHeight), 0).emit()
                .square(Direction.NORTH, x, y, x + modelWidth, y + modelHeight, z)
                .spriteBake(0, spriteGetter((tU += modelDepth), tV, modelWidth, modelHeight), 0).emit()
                .square(Direction.UP, z, y + modelHeight, z + modelDepth, y + modelHeight, z)
                .spriteBake(0, spriteGetter(tU, tV - modelHeight, modelWidth, modelDepth), 0).emit()
                .square(Direction.WEST, x + modelWidth, y, z + modelDepth, y + modelHeight, z)
                .spriteBake(0, spriteGetter((tU += modelWidth), tV, modelDepth, modelHeight), 0).emit()
                .square(Direction.SOUTH, x + modelWidth, y, x, y + modelHeight, z)
                .spriteBake(0, spriteGetter((tU += modelDepth), tV - modelHeight, modelWidth, modelHeight), 0).emit()
                .square(Direction.DOWN, x + modelWidth, y, z + modelDepth, y, z)
                .spriteBake(0, spriteGetter(defaultU + (modelWidth * 2), defaultV, modelWidth, modelDepth), 0).emit();
    }

    @Override
    public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {

    }

    @Override
    public ModelItemPropertyOverrideList getItemPropertyOverrides() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<BakedQuad> getQuads(BlockState arg0, Direction arg1, Random arg2) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Sprite getSprite() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ModelTransformation getTransformation() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean hasDepth() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isBuiltin() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isSideLit() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean useAmbientOcclusion() {
        // TODO Auto-generated method stub
        return false;
    }



    
}