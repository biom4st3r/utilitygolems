package com.biom4st3r.utilgolem;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.IronGolemEntityModel;

/**
 * UtilityGolemModel2
 */
public class UtilityGolemModel2 extends IronGolemEntityModel<UtilityGolemEntity> {

    public ModelPart torso;
    public UtilityGolemModel2() {
        super();
    }

    @Override
    public Iterable<ModelPart> getParts() {
        // Blocks
        torso = new ModelPart(this).setTextureSize(128, 128);
        this.torso.setPivot(0.0F, -7.0F, 0.0F);
        this.torso.setTextureOffset(0, 40).addCuboid(-9.0F, -2.0F, -6.0F, 18.0F, 2.0F, 11.0F, 0.0F); // Top plate
        this.torso.setTextureOffset(0, 40).addCuboid(-9.0F, 0.0F, -6.0F, 2.0F, 12.0F, 11.0F, 0.0F);// Left plate
        this.torso.setTextureOffset(0, 40).addCuboid(7.0F, 0.0F, -6.0F, 2.0F, 12.0F, 11.0F, 0.0F); // right plate
        this.torso.setTextureOffset(0, 50).addCuboid(-9.0F, 12.0F, -6.0F, 18.0F, 2.0F, 11.0F, 0.0F); // Bottom plate
        this.torso.setTextureOffset(0, 72).addCuboid(-4.5F, 14.0F, -3.0F, 9.0F, 1.0F, 6.0F, 0.5F);
        ImmutableList<ModelPart> list = (ImmutableList<ModelPart>) super.getParts();
        return ImmutableList.of(list.get(0),list.get(2),list.get(3),list.get(4),list.get(5),torso);
    }
    
}