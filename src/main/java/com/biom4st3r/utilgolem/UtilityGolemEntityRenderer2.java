package com.biom4st3r.utilgolem;

import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.util.Identifier;

/**
 * UtilityGolemEntityRenderer2
 */
public class UtilityGolemEntityRenderer2 extends MobEntityRenderer<UtilityGolemEntity, UtilityGolemModel2> {

    public UtilityGolemEntityRenderer2(EntityRenderDispatcher renderManager, UtilityGolemModel2 model, float f) {
        super(renderManager, model, f);
    }

    @Override
    protected void setupTransforms(UtilityGolemEntity entity, MatrixStack matrixStack, float animationProgress,
            float bodyYaw, float tickDelta) {
        super.setupTransforms(entity, matrixStack, animationProgress, bodyYaw, tickDelta);
        if ((double)entity.limbDistance >= 0.01D) {
            float j = entity.limbAngle - entity.limbDistance * (1.0F - tickDelta) + 6.0F;
            float k = (Math.abs(j % 13.0F - 6.5F) - 3.25F) / 3.25F;
            matrixStack.multiply(Vector3f.POSITIVE_Z.getDegreesQuaternion(6.5F * k));
        }
    }

    @Override
    public Identifier getTexture(UtilityGolemEntity entity) {
        // TODO Auto-generated method stub
        return new Identifier("textures/entity/iron_golem/iron_golem.png");
    }



    
}