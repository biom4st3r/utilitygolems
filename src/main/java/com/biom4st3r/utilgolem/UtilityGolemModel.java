package com.biom4st3r.utilgolem;

import com.google.common.collect.Lists;

import net.fabricmc.fabric.api.renderer.v1.Renderer;
import net.fabricmc.fabric.impl.client.indigo.renderer.render.BlockRenderContext;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.CompositeEntityModel;
import net.minecraft.client.util.math.MatrixStack;

/**
 * UtilityGolemModel
 */
public class UtilityGolemModel<E extends UtilityGolemEntity> extends CompositeEntityModel<E> {

    // ZombieEntityModel

    private ModelPart head;
    private ModelPart torso;
    private ModelPiece piece;
    // private final ModelPart blockEntity;
    // private final ModelPart leftArm;
    // private final ModelPart rightArm;
    // private final ModelPart leftLeg;
    // private final ModelPart rightLeg;
    // AbstractZombieModel
    // IronGolemEntityModel
    // public List<ModelPart> parts = Lists.newArrayList();
    public UtilityGolemModel()
    {

    }
    Renderer renderer;
    BlockRenderContext context = new BlockRenderContext();
    @Override
    public void render(MatrixStack matrices, VertexConsumer vertexConsumer, int light, int overlay, float red,
            float green, float blue, float alpha)
    {
        head = new ModelPart(this,0,0)
            .setTextureSize(128, 128)
            .addCuboid(0, -10, 0, 5, 7, 5);
        head.setPivot(-2.5f, -7f, -2.5f);
        torso = new ModelPart(this,0,12)
            .setTextureSize(128, 128)
            .addCuboid(-7.5f, -10, -2f, 15, 15, 4);
        torso.setPivot(0f, 0f, 0f);
        head.render(matrices, vertexConsumer, light, overlay);
        torso.render(matrices, vertexConsumer, light, overlay);
    }

    @Override
    public Iterable<ModelPart> getParts() {
        return Lists.newArrayList(head,torso);
    }

    @Override
    public void setAngles(E entity, float limbAngle, float limbDistance, float customAngle, float headYaw,
            float headPitch) {

    }

    
}