package com.biom4st3r.utilgolem;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityCategory;
import net.minecraft.entity.EntityType;
import net.minecraft.util.registry.Registry;

public class ModInit implements ModInitializer
{
	public static final String MODID = "utilgolem";
	@SuppressWarnings({"unchecked","rawtypes"})
	public static final EntityType<UtilityGolemEntity> Utility_Golem = 
		(EntityType)FabricEntityTypeBuilder
			.create(EntityCategory.CREATURE, (type,world)->
		{
			return new UtilityGolemEntity(world);
		}).build();

	@Override
	public void onInitialize() {
		Registry.register(Registry.ENTITY_TYPE, UtilityGolemEntity.packetid, Utility_Golem);
	}
}
