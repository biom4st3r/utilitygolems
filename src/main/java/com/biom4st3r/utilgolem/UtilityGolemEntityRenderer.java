package com.biom4st3r.utilgolem;

import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.util.Identifier;

public class UtilityGolemEntityRenderer extends MobEntityRenderer<UtilityGolemEntity, UtilityGolemModel<UtilityGolemEntity>> {

    public UtilityGolemEntityRenderer(EntityRenderDispatcher renderManager, UtilityGolemModel<UtilityGolemEntity> model,
            float f) {
        super(renderManager, model, f);
    }

    @Override
    public Identifier getTexture(UtilityGolemEntity entity) {
        return new Identifier(ModInit.MODID, "textures/entity/utilitygolem.png");
    }
}