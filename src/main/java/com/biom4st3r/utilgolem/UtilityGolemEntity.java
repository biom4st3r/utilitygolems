package com.biom4st3r.utilgolem;

import com.biom4st3r.biow0rks.entities.EntityPacket;

import net.minecraft.entity.passive.IronGolemEntity;
import net.minecraft.network.Packet;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

/**
 * UtilityGolemEntity
 */
public class UtilityGolemEntity extends IronGolemEntity {


    public UtilityGolemEntity(World world) {
        super(ModInit.Utility_Golem, world);
        // TODO Auto-generated constructor stub
    }

    public static final Identifier packetid = new Identifier(ModInit.MODID, "utilgolem");
    @Override
    public Packet<?> createSpawnPacket() {
        return EntityPacket.createSpawnPacket(this, packetid);
    }
    
}